import { Injectable } from '@nestjs/common';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
  private readonly users = [
    {
      id: 1,
      email: 'yoonhong@mail.com',
      password: 'yoonhong',
    },
    {
      id: 2,
      email: 'paewno@mail.com',
      password: 'paew',
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find((user) => user.email === username);
  }
}
